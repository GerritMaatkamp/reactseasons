import React from 'react';
import './SeasonDisplay.css';

const seasonConfig = {
    summer: {
        text: 'Shorts, T-shirt & flip-flops',
        iconName: 'sun',
        faIcon: 'fas fa-sun fa'
    },
    winter: {
        text: 'Grab a coat, it\'s cold!',
        iconName: 'sun',
        faIcon: 'fas fa-igloo'
    }
}

const getSeason = (lat, month) => {
    if (month > 2 && month < 9) {
        return lat > 0 ? 'summer' : 'winter';
    } else {
        return lat > 0 ? 'winter' : 'summer';
    }
    
}

const SeasonDisplay = (props) => {
    const season = getSeason(props.lat, new Date().getMonth());
    // const text = "";
    const {
        text,
        iconName,
        faIcon
    } = seasonConfig[season];

    console.log(season);
    let today = new Date();
    const months = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];

    return (
        
        <div className="container">
            <div className="small-icon">
                <i className={`${iconName}`}/>
            </div>
            <div className="icon">
                <div className={season}>
                    <i className={faIcon}></i>
                    <h1>{text}</h1>
                    <p className="display-output"> { months[today.getMonth()] }</p>
                </div>
            </div>
            <div className="small-icon right">
                <i className={`${iconName}`}/>
            </div>
        </div>
    )
}

export default SeasonDisplay;
