import React from 'react';
import './ErrorMessage.css';

const ErrorMessage = (props) => {
    return (
        <div className="error-message">
            <i class="fas fa-exclamation-triangle"></i> <p>{props.msg}</p> 
        </div>
    )
}

export default ErrorMessage;