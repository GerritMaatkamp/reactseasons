import React, { useState, useEffect  } from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import SeasonDisplay from './SeasonDisplay'
import Loader from './Loader'
import ErrorMessage from './ErrorMessage';
import useLocation from './useLocation'

// logic to locate user and current month

const App = () => {

    const [lat, errorMessage] = useLocation();
    let content;
    if(errorMessage) {
        content =  <div className="display"><ErrorMessage msg={errorMessage} /></div>;
    } else if (lat) {
        content = 
        <div className="season-display">
            <SeasonDisplay lat={lat}/>
            <p>Latitude: {lat}</p>
        </div>
    } else {
        content = <Loader message="Please accept location request" />;
    }

    return <div className="border red">{content}</div>
};


ReactDOM.render(<App />,document.querySelector('#root'));